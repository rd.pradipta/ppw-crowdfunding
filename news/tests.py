from django.urls import resolve
from django.utils import timezone
from django.test import TestCase, Client

from .views import news

class newsPageTest(TestCase):

    def test_news_url_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_template(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'news.html')