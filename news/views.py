from django.shortcuts import render
from assets.others.navbar_content import navigation_content
from news.models import NewsModel


def news(request):
    response = {
        "navigation" : navigation_content,
        "news": NewsModel.objects.all()
    }
    return render(request, 'news.html', response)
