from django.test import TestCase, Client


# Create your tests here.
from django.urls import resolve

from crowdfunding.views import home_page


class HomePageTest(TestCase):

    def test_home_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_page_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_homepage_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home_page.html')

    def test_homepage_render_the_result(self):
        test = 'MAKE LIFE BETTER'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
