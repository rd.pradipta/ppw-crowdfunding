import json

from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from assets.others.navbar_content import navigation_content
from crowdfunding.models import Testimony


def home_page(request):
    description = (
        """Yayasan SinarPerak adalah platform untuk menggalang dana untuk meringankan \
    beban dari korban bencana yang terjadi di Indonesia. Melihat banyaknya kasus bencana \
    yang sering terjadi. Yayasan SinarPerak memberikan wadah bagi masyarakat \
    agar dapat turut berpartisipasi memberikan bantuan kepada korban yang membutuhkan. """
    )

    response = {
        "header_title_top": "MAKE LIFE BETTER",
        "header_title_bottom": "BY HELPING EACH OTHER",
        "header_description": description,
        "navigation": navigation_content,
    }
    return render(request, "home_page.html", response)


def testimony_api(request):
    print("masuk")
    if request.method == 'POST':
        body = json.loads(request.body)
        print(body)
        testimony = Testimony(content=body['testimony'], username=body['username'])
        testimony.save()
        return HttpResponse(200)
    print("masuk get")
    testimony_list = []
    for testimony in Testimony.objects.all():
        testimony_list.append((testimony.content, testimony.username))
        print(testimony.content)
    return JsonResponse({"testimonys": testimony_list})


def about(request):
    return render(request, 'about.html')


def handler_404(request, exception):
    response = {}
    return render(request, '404.html', response)


def handler_500(request, exception):
    response = {}
    return render(request, '500.html', response)


def logout_action(request):
    logout(request)
    return HttpResponseRedirect('/')


def login_page(request):
    return render(request, "login.html")
