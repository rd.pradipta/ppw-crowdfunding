from django.test import TestCase, Client
from django.urls import resolve
from .models import ProgramModels
from .views import program_donasi_view, program_details_view

class ProgramDonasiTest(TestCase):
	def test_program_donasi_page_url_is_exist(self):
		response = Client().get('/program-donasi/')
		self.assertEqual(response.status_code, 200)

	def test_program_donasi_page_using_index_template(self):
		response = Client().get('/program-donasi/')
		self.assertTemplateUsed(response, 'programdonasi.html')

	def test_program_donasi_page_using_func(self):
		found = resolve('/program-donasi/')
		self.assertEqual(found.func, program_donasi_view)

	def test_model_can_create_program_donasi(self):
		new_program = ProgramModels.objects.create(nama='Donasiku', target='50000', total='5000',
			deskripsi='Ini program donasiku', 
			image_url='https://preview.ibb.co/b80QVf/blank-clean-color-889087.jpg')
		counting_all_program = ProgramModels.objects.all().count()
		self.assertEqual(counting_all_program, 1)