import datetime
from django import forms
from programdonasi.models import ProgramModels
DATE_INPUT_FORMATS = ['%Y-%m-%d']

class donation_form(forms.Form):
    PROGRAM_LIST = ((program.nama, program.nama) for program in ProgramModels.objects.all())
    programs = forms.ChoiceField(required=True, label='Nama Program' , choices=PROGRAM_LIST)
    donator_name = forms.CharField(max_length=255, required=True, label='Nama Lengkap')
    donator_email = forms.EmailField(max_length=255, label='Alamat E-mail')
    donator_amount = forms.IntegerField(required=True, label='Jumlah yang Hendak Didonasikan')

class Registration_Form(forms.Form):
    name = forms.CharField(max_length=255, required=True, label='Nama Lengkap')
    birthday = forms.DateField(initial=datetime.date.today,
                               input_formats=DATE_INPUT_FORMATS,
                               label='Tanggal Lahir',
                               widget=forms.DateInput(
                                   attrs={
                                       'class': 'form-control',
                                       'type': 'date',
                                   }
                               ))
    email = forms.CharField(max_length=255, required=True, label='Alamat E-mail')
    password = forms.CharField(
                max_length=255, required=True, label='Password',
                widget=forms.PasswordInput()
                )
