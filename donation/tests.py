from django.contrib.auth.models import User
from django.test import TestCase, Client

from donation.models import donation_list
# from .urls import app_name


class DonationPageTest(TestCase):

    # def test_donationPage_check_name(self):
    #     self.assertEqual(app_name, "donation_form")
    def setUp(self):
        user = User.objects.create_user('Dummy donator', 'dummy@dummy.id', 'Dummy donator')

    def test_donationPage_url_exist(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_donationPage_using_template(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response, 'form_donasi.html')

    def test_model_bisa_donasi(self):
        donation_list.objects.create(
            program_name="Dummy program",
            donator_name="Dummy donator",
            donator_email="dummy@dummy.id",
            donator_amount=2000000,
        )
        count_all_donation = donation_list.objects.all().count()
        self.assertEqual(count_all_donation, 1)


    # def test_homepage_render_the_result(self):
    #      self.client.login(username='Dummy donator', password='Dummy donator')
    #      donation_list.objects.create(
    #         program_name="Dummy program",
    #         donator_name="Dummy donator",
    #         donator_email="dummy@dummy.id",
    #         donator_amount=2000000,
    #      )
    #      response = self.client.get('/donasi/show')
    #      html_response = response.content.decode('utf8')
    #      self.assertIn("Dummy program", html_response)
    #      self.assertIn("2.000.000", html_response)
    #
    # def test_homepage_render_donation_sum(self):
    #      self.client.login(username='Dummy donator', password='Dummy donator')
    #      donation_list.objects.create(
    #         program_name="Dummy program2",
    #         donator_name="Dummy donator",
    #         donator_email="dummy@dummy.id",
    #         donator_amount=2000000,
    #      )
    #      donation_list.objects.create(
    #         program_name="Dummy program2",
    #         donator_name="Dummy donator",
    #         donator_email="dummy@dummy.id",
    #         donator_amount=50000,
    #      )
    #      response = self.client.get('/donasi/show')
    #      html_response = response.content.decode('utf8')
    #      self.assertIn("2.000.000", html_response)
    #      self.assertIn("50.000", html_response)
    #      self.assertIn("2.050.000", html_response)
    #      self.assertIn("Dummy program", html_response)
    #      self.assertIn("Dummy program2", html_response)
