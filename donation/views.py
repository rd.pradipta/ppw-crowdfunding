from django.contrib.auth.decorators import login_required
from django.views import View
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse

from .forms import donation_form, Registration_Form
from .models import donation_list, Donatur
from assets.others.navbar_content import navigation_content

@login_required
def donationShowPage(request):
    return render(request, 'view_donasi.html')


def donationAPI(request, email):
    donations = donation_list.objects.filter(donator_email=email)
    print(email)
    donations_list = []
    for donation in donations:
        print(donation.donator_amount)
        donations_list.append((donation.program_name, donation.donator_amount))
    return JsonResponse({"donations":donations_list})

class donationPage(View):
    template_file = "form_donasi.html"
    def get(self, request):
        response = {
            "title" : "DONASI",
            "navigation" : navigation_content,
            "donation_form" : donation_form(),
            "registration_form" : Registration_Form(),
        }
        print("Rendering page using HTML and responses")
        return render(request, self.template_file, response)

class donationAction(View):
    @csrf_exempt
    def post(self, request):
        program = request.POST['program_to_send']
        donator = request.POST['name_to_send']
        email = request.POST['email_to_send']
        amount = request.POST['amount_to_send']
        donation_list.objects.create(   program_name=program,
                                        donator_name=donator,
                                        donator_email=email,
                                        donator_amount=amount,
                                    ).save()
        data = {
            "donation_status" : True
        }
        return JsonResponse(data);
