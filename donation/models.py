import datetime
from django.db import models

class donation_list(models.Model):
    program_name = models.CharField(max_length=1000)
    donator_name = models.CharField(max_length=255)
    donator_email = models.EmailField(max_length=255)
    donator_amount = models.PositiveIntegerField()
    def __str__(self):
        return  self.donator_name + " " + self.program_name + " " + str(self.donator_amount)

class Donatur(models.Model):
    name=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    password=models.CharField(max_length=255)
    birthday=models.DateField()

    def __str__(self):
        return  self.name
