$(document).ready(function(){
    secureAJAX();

});

function secureAJAX(){
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

function csrfSafeMethod(method) { return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method)); }

function validateRegistrationForm(){
    var name = $('#id_name').val();
    var birthday = $('#id_birthday').val();
    var email = $('#id_email').val();
    var password = $('#id_password').val();

    // Bagian ini BELUM DIIMPLEMENTASIKAN DAFTAR YANG SEBENARNYA
    if (name !== '' && birthday !== '' && email !== '' && password !== ''){
        pushSuccess("Pendaftaran Berhasil! Silakan Login dengan Google!");
    }
    else{
        pushError("Pendaftaran Gagal! Pastikan Anda memasukkan data diri dengan benar");
    }
    $('#panel').css("height","800px");
}

function validateDonateForm(){
    var program = $('#id_programs').val();
    var name = $('#id_donator_name').val();
    var email = $('#id_donator_email').val();
    var amount = $('#id_donator_amount').val();

    if (name !== '' && email !== '' && amount !== ''){
        $.post( "mulai-donasi/", {
            "program_to_send": program,
            "name_to_send": name,
            "email_to_send": email,
            "amount_to_send": amount,
        },"json")
        // Pemanggilan AJAX sukses
        .done(function(data) {
            let result = (data.donation_status) ? pushSuccess("Donasi Sukses! Silakan lakukan pembayaran") : pushError("Donasi Gagal!");
        })
        // Pemanggilan AJAX gagal, umumnya karena masalah sistem & internet
        .fail(function(data){
            AJAXFail(data);
        });
        $('#panel').css("height","670px");
        url: '/show'
        setTimeout(function(){window.location = url;}, 5000); 

    }
    else{
        pushError("Mohon lengkapi data dengan benar!");
    }
}

function pushError(msg){
    $("#successCard").addClass("d-none");
    $("#errorCard").removeClass("d-none");
    $("#error").text(msg);
}

function pushSuccess(msg){
    $("#errorCard").addClass("d-none");
    $("#successCard").removeClass("d-none");
    $("#success").text(msg);
}

function AJAXFail(data){
    pushError("Terjadi galat! Periksa kembali koneksi internet Anda!");
    setTimeout(function(){
        $("#errorCard").addClass("hide");
    }, 5000);
}